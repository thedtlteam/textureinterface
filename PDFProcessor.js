//Maeda Hanafi
//PDF processor

var PDFProcessor = function(infilename){
	var filename = infilename ;	//filename
	var pdfAndEntities;
	
	//This module is used to solve the serialization of circular jsons
	var CircularJSON = require('circular-json');
	
	//All information we want to collect from pdf	
	var  pdfjson; 
	var totaltext = "";
	
	//AlchemyAPI API key
	var APIKey = "37abd9121c9dc242fdd73073c0f68b935e6631a3";

	var events = require('events');
	var eventEmitter = new events.EventEmitter();
	
	//Variables relating to sending to websocket
	//This is the one to signal the websocket to send the data
	var eventemittersocket;
	var socket;
	var handler;
	
	//Begins the processing
	var process =function(ineventemitter, insocket, inhandler){
		eventemittersocket = ineventemitter;
		socket = insocket;
		handler = inhandler;
		
 		console.log("Processing the pdf");
		
		//Once parsePDF is done, an event is emitted to call the NER function
		//Once the NER fuunction is done, another event is emitted, but this time to the websocket
		eventEmitter.on('parsePDF_done', NER);
		
		//Calling the first function
		parsePDF();		
		 
	};
	
	//Returns result 
	var getResult = function(){
 		return pdfAndEntities;
	};
	
	
	//Parses the PDF  and sends it to extractText() 
	var parsePDF = function ( ){
		var nodeUtil = require("util"),
			fs = require('fs'),
			_ = require('underscore'),
			PDFParser = require("./node_modules/pdf2json/pdfparser");
			
		//Using this library we convert PDF to JSON
		var pdfParser = new PDFParser();
		//Once the data is ready we want to get the pure text from the json and then NER on it
		pdfParser.on("pdfParser_dataReady", _.bind(extractText)); 	
		
 		pdfParser.loadPDF(filename);
		
		socket.emit('pong');
		  
	};

	//Given json data, it will concatenate all the text and then send it to NER() 
	var extractText = function(data){
		
		pdfjson = data.PDFJS;
		var pages = pdfjson.pages;
		
		//Concatenating all text from the json result		
		pages.forEach(function(page) {			 
			page["Texts"].forEach(function(textinfo) {
				textinfo["R"].forEach(function(text) {
					totaltext = totaltext+"%20"+text["T"];
				});
			});
		});
		
		//Done getting pure string text from the pdf
		totaltext = decodeURIComponent(totaltext);
		
		console.log("Done extracting text from "+filename);
 		
		eventEmitter.emit('parsePDF_done');
		
		socket.emit('pong');
		
		return;
	};

	var NER = function (){
		console.log("Contacting Alchemy Servers...");
		
		var AlchemyAPI = require('alchemy-api');
		var alchemy = new AlchemyAPI(APIKey);
 		alchemy.entities(totaltext, {}, function(err, response) {
			socket.emit('pong');
			
			if (err){ 
				console.log(err);
				return ;
			}else{

				// See http://www.alchemyapi.com/api/entity/htmlc.html for format of returned object
				var entities = response.entities;
				
				//Serialize pdfjson, or else circular json error, which in turn causes "maximum call stack exceeded" error will occur
				var serialized = CircularJSON.stringify(pdfjson);
				
				pdfAndEntities = {
					pdfjson:serialized,
					pdftext:totaltext,
					entities:entities,
					file:"../documents/"+filename
				};
				 
				console.log("Done with identifying named entities of "+filename);
				
				//Signal the websocket to send the data over to 
				eventemittersocket.emit('processPDF_done', socket, handler, pdfAndEntities);
				 
				return;
			}
		});
		return;
	};

	var displayError = function(err){
		console.log(err);
	}
	 
	return  {
	
		getResult: getResult,
		process: process 
	}
};
//allow access for other files to access this
exports.PDFProcessor = PDFProcessor;


