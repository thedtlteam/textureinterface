/* 
 * socketio.js
 * 
 * Socket io functions for dealing with client requests
 */

 //Event emitter to control the flow of pdf processing
var events = require('events');
var eventEmitter = new events.EventEmitter();

//The PDFProcessor
var pdfprocessorImport = require("./PDFProcessor").PDFProcessor;

var setFunctions = function(socket){
 	socket.on("pdfinfo", onPDFInfo);
	
};

var onPDFInfo = function(data){
	var thissocket = this;
 	console.log("Sending client pdf info:"+this.id);
 
	//Create an event emitter listener
	eventEmitter.on('processPDF_done', sendDataToClient );
	//eventEmitter.on('NER_done', putInDatabase )

	//Call the pdf processor 
	var pdfprocessor = new pdfprocessorImport("static/documents/"+data.filename);
	pdfprocessor.process(eventEmitter, this, "pdfinfoback");
}

var sendDataToClient = function(socket, handler, jsonMessage){ 
	//console.log(jsonMessage);
 	socket.emit(handler, jsonMessage);
 };
 
exports.init = function(server) {
 	var io = require('socket.io')(server);
	
	console.log("Successful init for socket.io"); 
	
	io.on('connection', function(socket){ 
		setFunctions(socket);
		socket.emit('message', {filename: "filename.pdf"});
		console.log('Socket.io Connection with the client established');
	});
	
    return io;
}