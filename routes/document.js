
/*
 * Route for document view
 */
 
var document = function(inlocals  ){
	var locals = inlocals;
	var fs=require('fs');
	var ejs = require('ejs');
	 
	function getmain(req, res){ 
		//Fill with array of files
		locals.files = ["MaedaHanafiResume2014.pdf", "kevin_resume.pdf", "sample_resume.pdf"]; 
		locals.collection = ["MaedaHanafiResume2014.pdf", "kevin_resume.pdf"]; 
		locals.labels = [];
		
		var file2=__dirname+'/../views/documentlist.ejs';
		var ejs_file2 = fs.readFileSync(file2, 'utf-8');		
		locals.documentlist = ejs.render(ejs_file2, {files:locals.files});
		
		var file1=__dirname+'/../views/collectionlist.ejs';
		var ejs_file1 = fs.readFileSync(file1, 'utf-8');		
		locals.collectionlist = ejs.render(ejs_file1, {collection:locals.collection  });
			
		res.render('document.ejs', locals);
	}; 
	return  {
 		getDocumentView:getmain
 	}
};
//allow others to access this file
exports.document = document;
