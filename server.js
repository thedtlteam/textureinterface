/* 
 * server.js
 * 
 * The main file, to be invoked at the command line. 
 */


var port = 8000;
var locals = {
        title: 		 'Node | Express | EJS | Boostrap',
        description: 'A Node.js applicaton bootstrap using Express 3.x, EJS, Twitter Bootstrap, and CSS3',
        author: 	 'C. Aaron Cois, Alexandre Collin',
        _layoutFile: true
    };
	
 
var express = require('express')
	, engine = require('ejs-locals')
   , app = express();
var server = require('http').Server(app);

app.configure(function(){
	app.set('views', __dirname + '/views');
	app.set('view engine', 'ejs');
	app.use(express.bodyParser());
	app.use(express.methodOverride());
	app.use(express.static(__dirname + '/static'));
	app.use(app.router);
	app.enable("jsonp callback");
	
 
});

app.engine('ejs', engine);

app.configure('development', function(){
   app.use(express.errorHandler({ dumpExceptions: true, showStack: true })); 
	// app.use(express.logger({ format: ':method :url' }));
});

app.configure('production', function(){
   app.use(express.errorHandler()); 
});


app.use( function(err, req, res, next) {
	res.render('500.ejs', { locals: { error: err }, status: 500 });
});


//Routes Initialized
var documentroute = new require('./routes/document').document(locals);

//Setting Routes	
app.get('/', documentroute.getDocumentView);

//The 404 Route (ALWAYS Keep this as the last route) *
app.get('/*', function(req, res){
    res.render('404.ejs', locals);
});

server.listen(port);
